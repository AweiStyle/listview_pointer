package com.example.awei.listviewpractice_pointer;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.util.Base64;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

public class inputActivity extends AppCompatActivity {

    EditText editText1;
    EditText editText2;
    EditText editText3;

    private static final int CAMERA_REQUEST = 1888;
    private ImageView imageView;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;

    String[][] data;
    Gson gson = new Gson();
    ArrayList<String[]> arrayList;
    Bitmap bitmap;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input);

        Button send = findViewById(R.id.BT_1);

        editText1 = findViewById(R.id.ET_1);
        editText2 = findViewById(R.id.ET_2);
        editText3 = findViewById(R.id.ET_3);


//        final String[][] data = {{"asd","qwe","zxc", "https://img4.momoshop.com.tw/goodsimg/0005/912/010/5912010_L.jpg?t=1545886104"},
//                {"111","222","333","https://shop.r10s.com/009d6890-ec8c-11e4-b5b5-005056b70c54/ab/ab3354.jpg"},
//                {"sss", "ddd","aaa",""}};
//
//
//        SharedPreferences pref = getSharedPreferences("data",MODE_PRIVATE);  //讀取已儲存資料
//        Gson gson = new Gson();
//        String json = gson.toJson(data);    //把陣列轉換成字串
//        pref.edit().putString("data",json).commit();  //把字串寫入data陣列
//
//        ArrayList myList = gson.fromJson(json,ArrayList.class);


        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String ET_name = editText1.getText().toString();
                final String ET_cost = editText2.getText().toString();
                final String ET_price = editText3.getText().toString();

                String a = getSharedPreferences("data", MODE_PRIVATE).getString("data", "");

                if (a.equals("")) {

                    data = new String[][]{};

                } else {


                    data = gson.fromJson(a, String[][].class);

                }



                arrayList = new ArrayList(Arrays.asList(data));
                arrayList.add(new String[] { ET_name , ET_cost , ET_price});
                data = arrayList.toArray(new String[0][0]);


                String c = gson.toJson(data);


                SharedPreferences sharedPreferences = getSharedPreferences("data", MODE_PRIVATE);
                sharedPreferences.edit().putString("data", c).commit();


                finish();

            }


        });


    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void openCamera(View view) {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
        } else {
            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(cameraIntent, CAMERA_REQUEST);
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {

            bitmap = (Bitmap) data.getExtras().get("data");

            ImageView imageView = findViewById(R.id.image_1);
            imageView.setImageBitmap(bitmap);


        }
    }
}
