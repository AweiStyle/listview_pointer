package com.example.awei.listviewpractice_pointer;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;

public class Main2Activity extends AppCompatActivity {

    ListView listView;
    String[][] data;
    Gson gson;
    ArrayList<String> arrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);


        String data1 = getSharedPreferences("data",Context.MODE_PRIVATE).
                getString("data","");

        gson = new Gson();
        String[][] items = gson.fromJson(data1,String[][].class);




        if (items == null) items = new String[][] {};




        listView = findViewById(R.id.listView);
        ViewAdapter adapter = new ViewAdapter(items);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

                gson = new Gson();

                String a = getSharedPreferences("data", MODE_PRIVATE).getString("data", ""); //讀取

                if (a.equals("")) {         //55~56行 如果讀取到空字串 就新增一個空陣列 避免 crash

                   data = new String[][]{};

                } else {                    //59~64行 如果讀取時有資料 就把讀取的字串轉換成陣列顯示


                    data = gson.fromJson(a, String[][].class);

                }

                arrayList = new ArrayList(Arrays.asList(data));  //把array轉換成arrayList
                                                                                   //arrayList才能做陣列刪減

                final AlertDialog.Builder builder = new AlertDialog.Builder(Main2Activity.this);
                builder.setTitle("刪除資料");
                builder.setMessage("確定要刪除這筆資料嗎？");
                builder.setIcon(R.drawable.ic_launcher_background);
                builder.setPositiveButton("是", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        arrayList.remove(position);     //刪除點擊的列表項目

                        data = arrayList.toArray(new String[0][0]);  //轉換回array 因為原先就是用array連結listView
                        String a = gson.toJson(data);

                        SharedPreferences sharedPreferences = getSharedPreferences("data",MODE_PRIVATE);  //73~74寫入資料
                        sharedPreferences.edit().putString("data",a).commit();

                        ViewAdapter adapter = new ViewAdapter(data);  //77~78行 呼叫adapter來顯示內容
                        listView.setAdapter(adapter);
                    }
                });

                builder.setNegativeButton("否", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();

                    }

                });

                builder.show();



                data = arrayList.toArray(new String[0][0]);  //轉換回array 因為原先就是用array連結listView
                a = gson.toJson(data);

                SharedPreferences sharedPreferences = getSharedPreferences("data",MODE_PRIVATE);  //73~74寫入資料
                sharedPreferences.edit().putString("data",a).commit();


                ViewAdapter adapter = new ViewAdapter(data);  //77~78行 呼叫adapter來顯示內容
                listView.setAdapter(adapter);



            }
        });



    }


}
