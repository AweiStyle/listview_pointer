package com.example.awei.listviewpractice_pointer;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.io.ByteArrayOutputStream;

public class ViewAdapter extends BaseAdapter {

    String items[][];
    String encoded;


    public ViewAdapter(String[][] items){

        this.items = items;

    }

    @Override
    public int getCount() {
        return items.length;
    }

    @Override
    public Object getItem(int position) {
        return items[position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_example,null);

        TextView item = view.findViewById(R.id.item);
        TextView cost = view.findViewById(R.id.cost);
        TextView price = view.findViewById(R.id.price);

        item.setText(items[position][0]);  //以陣列為單位顯示內容
        cost.setText(items[position][1]);
        price.setText(items[position][2]);

        ImageView pic = view.findViewById(R.id.pic);
        // Glide.with(parent.getContext()).load(items[position][3]).into(pic);


        return view;



    }
}
